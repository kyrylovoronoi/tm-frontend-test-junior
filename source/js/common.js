/*Global variables*/
var currentScrollTop,
	currentScrollBottom,
	windowHeight,
	dataForRender = [];

;(function() {
	$.fn.mySlider = function(config) {
		var _this = this;

		_this.currentSlide = 0;
		_this.lastSlide;
		_this.slides = $('li', _this);
		_this.interval = null;
		_this.isAutoplay;
		_this.autoplayDelayMS;

		config || (config = {});

		if(config.isAutoplay){
			_this.isAutoplay = config.isAutoplay;
		}else{
			_this.isAutoplay = false;
		}

		if(config.autoplayDelayMS){
			_this.autoplayDelayMS = config.autoplayDelayMS;
		}else{
			_this.autoplayDelayMS = 4000;
		}

		_this.init = function(){
			if(_this.slides.length){
				_this.slides.css({
					'position' : 'absolute', 
					'display' : 'none'
				});
				_this.slides.eq(0).addClass('current')
				.css({
					'position' : 'relative',
					'display' : ''
				});
			}

			_this
				.wrap('<div class="slider-wrapper"></div>')
				.parent()
				.prepend('<button class="prev"></button>')
				.append('<button class="next"></button>');

			_this.prev().on('click', { 'direction' : 'prev' }, _this.move);

			_this.next().on('click', _this.move);

			if(_this.isAutoplay){
				_this.autoplay();
			}
		};

		_this.move = function(e) {
			e || (e = {});

			_this.lastSlide = _this.currentSlide;

			_this.slides
			.eq(_this.lastSlide)
			.css({
				'z-index': '8',
				'position' : 'absolute', 
				'display' : 'none'
			});

			if(e.data && e.data.direction === 'prev'){
				if(_this.currentSlide == 0) {
					_this.currentSlide = _this.slides.length - 1;
				}else{
					_this.currentSlide--;
				}

				_this.slides
				.eq(_this.currentSlide)
				.addClass('current')
				.css({
					'animation-name' : 'slide-in-left', 
					'position' : 'relative',
					'display' : ''
				});
			}else{
				if((_this.currentSlide + 1) == _this.slides.length) {
					_this.currentSlide = 0
				}else{
					_this.currentSlide++;
				}

				_this.slides
				.eq(_this.currentSlide)
				.addClass('current')
				.css({
					'animation-name' : 'slide-in-right', 
					'position' : 'relative',
					'display' : ''
				});
			}

			setTimeout(function(){
				_this.slides
				.eq(_this.lastSlide)
				.removeClass('current')
				.css({
					'animation-name' : '', 
					'z-index': ''
				});
			}, 500);

			if(_this.isAutoplay){
				if(_this.interval){
					clearInterval(_this.interval);
				}

				_this.autoplay();
			}
		};
		
		_this.autoplay = function() {
			_this.interval = setInterval(_this.move, _this.autoplayDelayMS);
		};

		_this.init();
    };

    $.fn.myParallax = function(config) {
    	var _this = this;

    	_this.sectionHeight;
        _this.sectionOffsetYTop;
        _this.sectionOffsetYBottom;
        _this.isVisibleSection;
        _this.parallaxStep;

        _this.parallaxItems = [];

		config || (config = {});

		if (config.parallaxItems) {
            _this.parallaxItems = config.parallaxItems.slice();

            _this.parallaxItems.map(function(item, index, array){
            	if(!item.itemSelector){
					return;
            	}

            	if(!(parseInt(item.stepDenominator))){
            		item.stepDenominator = 10;
            	}

            	item.itemPositionDefault = parseInt($(item.itemSelector).css('transform').split(',')[5]);

            	if(!item.itemPositionDefault){
            		item.itemPositionDefault = 0;
            	}
            });
        } else {
            return;
        }

        function initSectionValuesForParallax() {
            _this.sectionHeight = _this.outerHeight();
            _this.sectionOffsetYTop = _this.offset().top;
            _this.sectionOffsetYBottom = _this.sectionOffsetYTop + _this.sectionHeight;
            _this.isVisibleSection = checkIsVisibleParallaxSection();

            if (_this.isVisibleSection) {
            	_this.parallaxItems.map(function(item){
            		parallaxMotion(item);
            	});
            }
        }

        function checkIsVisibleParallaxSection(){
        	if(((currentScrollTop < _this.sectionOffsetYTop) && (currentScrollBottom > _this.sectionOffsetYTop)) || 
				((currentScrollTop < _this.sectionOffsetYBottom) && (currentScrollBottom > _this.sectionOffsetYBottom))){
	        	return true;
	        }else{
	        	return false;
	        }
        }

        function parallaxMotion(item){
        	item.parallaxStep = (item.itemPositionDefault + ((currentScrollTop + windowHeight - _this.sectionOffsetYTop) / item.stepDenominator));
                $(item.itemSelector).css({
                    'transform' : 'translateY(' + item.parallaxStep + 'px)'
            });
        }

        initSectionValuesForParallax();

		$(window).on("resize", function() {
		    initSectionValuesForParallax();
		});
           
        $(window).scroll(function() {
        	 _this.isVisibleSection = checkIsVisibleParallaxSection();

            if (_this.isVisibleSection) {
            	_this.parallaxItems.map(function(item){
            		parallaxMotion(item);
            	});
            }
        }); 
    };
}());

function getHouseInfo(){
	var allHouses = [],
		housesArray = [
			'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=1',
			'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=2',
			'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=3',
			// 'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=4',
			// 'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=5',
			// 'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=6',
			// 'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=7',
			// 'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=8',
			// 'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=9',
			'https://www.anapioficeandfire.com/api/houses?pageSize=49&page=10'
		],
		requests = housesArray.map(function(request) {
		    return $.ajax({
		        method: 'GET',
		        url: request,
		        accepts: 'application/vnd.anapioficeandfire+json; version=1'
		    });
		});

	$.when.apply(this, requests)
	.then(function(){
		if(arguments[1] == 'success'){
			for (var i = 0; i < arguments[0].length; i++) {
				allHouses.push(arguments[0][i]);
			}
		}else{
			for (var i = 0; i < arguments.length; i++) {
				for (var j = 0; j < arguments[i][0].length; j++) {
					allHouses.push(arguments[i][0][j]);
				}
			}
		}
	}).then(function(){
		if(allHouses.length){
			getDataCharacters(allHouses);
		}
	});

	function getDataCharacters(housesData){
		// var housesData = $.parseJSON(housesData);
		var checker = 0;

		$.each(housesData, function(houseIndex, house){
			dataForRender[houseIndex] = {};

			if(house.name.length){
				dataForRender[houseIndex].name = house.name;
			}else{
				dataForRender[houseIndex].name = 'Unknown';
			}

			if(house.region.length){
				dataForRender[houseIndex].region = house.region;
			}else{
				dataForRender[houseIndex].region = 'Unknown';
			}
			
			dataForRender[houseIndex].residents = [];

			if(house.swornMembers.length){
				
				requests = house.swornMembers.map(function(request) {
				    return $.ajax({
				        method: 'GET',
				        url: request,
				        accepts: 'application/vnd.anapioficeandfire+json; version=1'
				    });
				});

				$.when.apply(this, requests)
				.then(function(){
					if(arguments[1] == 'success'){
						if(arguments[0].name.length){
							dataForRender[houseIndex].residents.push(arguments[0].name);
						}else{
							dataForRender[houseIndex].residents.push('Unknown');
						}
					}else{
						for (var i = 0; i < arguments.length; i++) {
							if(arguments[i][0].name.length){
								dataForRender[houseIndex].residents.push(arguments[i][0].name);
							}else{
								dataForRender[houseIndex].residents.push('Unknown');
							}
						}
					}
				}).then(function(){
					checker++;
				    
				    if(checker == housesData.length){
				    	renderHouseInfoTable(dataForRender)
				    }
				});
			}else{
				checker++;
			}
		});
	}
}

function renderHouseInfoTable(data){
		var table = $('.table-wrapper table tbody'),
			dataString,
			residents;

		table.empty();

		for (var i = 0; i < data.length; i++) {
			if(data[i].residents.length){
				residents = '<ul class="residents-list"><li>'+ data[i].residents.join('</li><li>') + '</li></ul>';
			}else{
				residents = '<ul class="residents-list"><li>No one</li></ul>';
			}
			
			dataString = dataString + '<tr><td>' + data[i].name + '</td><td>' + data[i].region + '</td><td>' + residents + '</td></tr>';
		}

		table.append(dataString);
}

function sortBy(field, reverse, origin){
   	var key = function(x) {
   		return origin ? origin(x[field]) : x[field]
   	};

   	return function(a,b) {
	  	var A = key(a), B = key(b);
	  	return ( (A < B) ? -1 : ((A > B) ? 1 : 0) ) * [-1,1][+!!reverse];                  
   	}
}

function initSortDataTable(){
	var nameTitleButton = $('.name-col-title'),
	    regionTitleButton = $('.region-col-title');

	nameTitleButton.on('click', function(e){
		e.preventDefault();

		_this = $(this);

		if(!_this.hasClass('desc')){
			dataForRender.sort(sortBy('name', false, function(a){return a.toUpperCase()}));
			_this
			.removeClass('asc')
			.addClass('desc');
		}else{
			dataForRender.sort(sortBy('name', true, function(a){return a.toUpperCase()}));
			_this
			.removeClass('desc')
			.addClass('asc');
		}
		
		renderHouseInfoTable(dataForRender);
	})

	regionTitleButton.on('click', function(e){
		e.preventDefault();

		_this = $(this);

		if(!_this.hasClass('desc')){
			dataForRender.sort(sortBy('region', false, function(a){return a.toUpperCase()}));
			_this
			.removeClass('asc')
			.addClass('desc');
		}else{
			dataForRender.sort(sortBy('region', true, function(a){return a.toUpperCase()}));
			_this
			.removeClass('desc')
			.addClass('asc');
		}
		
		renderHouseInfoTable(dataForRender);
	})
}

function initGlobalVariables(event){
	if(event != 'scroll'){
		windowHeight = $(window).outerHeight();
	}

	currentScrollTop = $(window).scrollTop();
	currentScrollBottom = currentScrollTop + windowHeight;
}

function initFormValidation(form) {
    var form = $(form).eq(0),
        inputsCollection = $('input:not([type="hidden"])', form),
        inputsValidateCollection = $('input[data-validate="name"], input[data-validate="email"], ' +
        	'input[data-validate="phone"], input[data-validate="password"], ' + 
        	'input[data-validate="confirm"], [data-validate="textarea"]', form),
        buttonSubmit = $('[type="submit"]', form),
        previousValueLocal,
        errorMessage;
 
    inputsValidateCollection.map(function(item) {
        if (/name/i.test($(this).attr('data-validate'))) {
            $(this).prop('regexp', '^[a-z ]*$');
            $(this).prop('message', 'Latin letters only!');
        }

        if (/email/i.test($(this).attr('data-validate'))) {
            $(this).prop('regexp', '^.+@.+\.+[a-zA-Z]{2,4}$');
            $(this).prop('message', 'Incorrect e-mail!');
        }

        if (/phone/i.test($(this).attr('data-validate'))) {
            //$(this).prop('regexp', '^([+]?[0-9\s-\(\)]{2,25})*$');
            $(this).prop('regexp', '^[+]?[0-9]*$');
            $(this).prop('message', 'Incorrect telephone number!');
        }

		if (/textarea/i.test($(this).attr('data-validate'))) {
            $(this).prop('regexp', '^.{10,3000}$');
            $(this).prop('message', 'Must be between 10 and 3000 characters!');
        }

        if (/password/i.test($(this).attr('data-validate'))) {
            $(this).prop('regexp', '(?=.*[!@#$%^&*])(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9!@#$%^&*a-zA-Z]{6,}');
            $(this).prop('message', 'Must have at least 1 small, big, special type & over 6 characters!');
        }

        if (/confirm/i.test($(this).attr('data-validate'))) {
            $(this).prop('message', 'Passwords don\'t match!');
        }
    });

    function initInputsAutocompleteOff() {
        $(inputsCollection).map(function(item) {
            $(this).attr('autocomplete', 'off');

            $(this).on('contextmenu', function(e) {
                e.preventDefault();
            });

            $(this).on('paste', function(e) {
                e.preventDefault();
            });
        });
    }
 
    function addErrorMessage(input, message) {
        if(!input.next().hasClass('error-message')){
			input.after('<div class="error-message">' + message + '</div>');
		}

        input
        .addClass('no-valid')
        .addClass('no-valid-regexp')
        .prop('valid', 'false');
    }
 
    function removeErrorMessage(input) {
        input = $(input);
        errorMessage = input.parent().children('.error-message');

        if (errorMessage.length > 0) {
            errorMessage.remove();
        }

        if (input.hasClass('no-valid')) {
            input.removeClass('no-valid');
        }

        if (input.hasClass('no-valid-regexp')) {
            input.removeClass('no-valid-regexp');
        }

        input.prop('valid', 'true');
    }
 
    function checkInputRequired(input) {
        input = $(input);

        if (input.val() == '') {
            addErrorMessage(input, 'Please specify this field');
        }
    }
 
    function checkTextRegexp(input) {
        input = $(input);
        var regexp = new RegExp(input.prop('regexp'), i);
 
        if (/email/i.test(input.attr('data-validate')) || /textarea/i.test(input.attr('data-validate'))) {
            if (!regexp.test(input.val().toLowerCase())) {
                addErrorMessage(input, input.prop('message'));
                // input.addClass('no-valid-regexp');
            }
        } else if(/password/i.test(input.attr('data-validate'))){
        	if (!regexp.test(input.val())) {
                addErrorMessage(input, input.prop('message'));
                // input.addClass('no-valid-regexp');
            }else {
                removeErrorMessage(input);
            }
        }else if(/confirm/i.test(input.attr('data-validate'))){
        	if (input.val() != inputsValidateCollection.filter('[name="password"]').val()) {
                addErrorMessage(input, input.prop('message'));
                // input.addClass('no-valid-regexp');
            }else {
                removeErrorMessage(input);
            }
        }else {
            previousValueLocal = input.prop('previousValue');

            if (!regexp.test(input.val().toLowerCase())) {
                addErrorMessage(input, input.prop('message'));
                // input.addClass('no-valid-regexp');

                if (previousValueLocal == undefined || previousValueLocal == '') {
                    input.val(input.val().slice(0, -1));
                } else {
                    for (var i = 0; i < previousValueLocal.length; i++) {
                        if (previousValueLocal[i] != input.val()[i]) {
                            input.val(input.val().slice(0, i) + input.val().slice(i + 1, input.val().length));
                            break;
                        }

                        if (i == previousValueLocal.length - 1 &&
                            previousValueLocal[previousValueLocal.length - 1] == input.val()[previousValueLocal.length - 1]) {
                            input.val(input.val().slice(0, -1));
                        }
                    }
                }

                setTimeout(function(obj) {
                    obj.removeClass('no-valid-regexp');
                }, 200, input);
            } else {
                removeErrorMessage(input);
            }
            input.prop('previousValue', input.val());
        }
    }
 
    function initInputsValidation(inputsCollection) {
        inputsCollection.on('focus', function() {
            removeErrorMessage(this, false);
        });

        inputsCollection.on('input', function() {
            if (/email/i.test($(this).attr('data-validate'))) {
                return;
            }

			if (/textarea/i.test($(this).attr('data-validate'))) {
                return;
            }
            checkTextRegexp(this);
        });
        inputsCollection.on('blur', function() {
            if (/email/i.test($(this).attr('data-validate')) && $(this).val() != '') {
                checkTextRegexp(this);
                return;
            }

            if (/password/i.test($(this).attr('data-validate')) && $(this).val() != '') {
                checkTextRegexp(this);
                return;
            }

            if (/confirm/i.test($(this).attr('data-validate')) && $(this).val() != '') {
                checkTextRegexp(this);
                return;
            }

			if (/textarea/i.test($(this).attr('data-validate')) && $(this).val() != '') {
                checkTextRegexp(this);
                return;
            }

            removeErrorMessage(this);

            if ($(this).prop('required')) {
                checkInputRequired(this);
            }
        });
    }
 
    function ajaxResponseCallbackHandler(button, message) {
        form.append('<div class="response-success-message"><span>' + message + '</span><span>Our time together will be awesome!</span></div>');

        $(button).html('already sent!');
 
        setTimeout(function() {
            $('.response-success-message', form).addClass('message-show');
        }, 50);
 
        setTimeout(function() {
            //$('.response-success-message', form).removeClass('message-show');
            //$(button).removeAttr('disabled');
            $('.signin').trigger('click').text(inputsValidateCollection.filter('[name="name"]').val());
        }, 5000);
 
        // setTimeout(function() {
        //     $('.response-success-message', form).remove();
        // }, 5500);
    }
 
    function initButtonSubmit(buttonSubmit) {
        buttonSubmit.on('click', function(e) {
            e.preventDefault();

			$(this).blur();
 
            var isValid = true;

            $(inputsValidateCollection).map(function(item) {
                if ($(this).prop('required') && $(this).val() == '') {
                    checkInputRequired(this);
                }
                if ($(this).prop('valid') == 'false') {
                    isValid = false;
                }
            });
 
            if (isValid) {
                var button = $(this);
                button.html('Sending...');
                button.attr('disabled', 'disabled');
 
                //$.ajax({
                //    url: 'sender.php',
                //    type: 'post',
                //    data: {
                //        'name': $('input[data-validate="name"]', form).val(),
                //        'email': $('input[data-validate="email"]', form).val()
                //    },
                //    success: function(data) {
                //        ajaxResponseCallbackHandler(button, 'Thanks for your message!<br/> We got it!');
                //    },
                //    error: function() {
                //        ajaxResponseCallbackHandler(button, 'Something was wrong...<br/> Please, try again...');
                //    }
                //});
 
                //test without sending
                ajaxResponseCallbackHandler(button, 'thank you <br> for joining us!');
            }
        });
    }
 
    initInputsAutocompleteOff();
    initInputsValidation(inputsCollection);
    initButtonSubmit(buttonSubmit);
}

function setFocusOnSinginFormShow(){
	$('.signin').on('click', function(e){
		e.preventDefault();
		
		if($(this).attr('aria-expanded') != 'true'){
			setTimeout(function(){
				$('[name="name"]').focus();
			}, 300);
		}
	});
}

function initEvents() {
    /*Actions on 'DOM ready' event*/
    $(function() {
    	initGlobalVariables();
    	$('.templates-list').mySlider({
    		'isAutoplay' : true,
    		'autoplayDelayMS' : 4000
    	});
    	$('.banner').myParallax({
    		parallaxItems : [{
    			itemSelector : '.heart',
    			stepDenominator : -15
    		}, {
    			itemSelector : '.box',
    			stepDenominator : 15
    		}, {
    			itemSelector : '.lines',
    			stepDenominator : 5
    		}]
    	});
    	getHouseInfo();
    	initSortDataTable();
    	initFormValidation('form');
    	// setFocusOnSinginFormShow();
    });

    $(window).on("load", function() {
    });

    $(window).on("scroll", function(e) {
    	initGlobalVariables(e.type);
    });

    $(window).on("resize", function() {
    	initGlobalVariables();
    });
};

/*Start all functions and actions*/
initEvents();