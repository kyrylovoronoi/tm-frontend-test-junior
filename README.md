# Gulp Starterkit


## About 
This is a useful starter kit to help you prototype faster. It follows the
opinionated styles and includes our favorite front end tools.


Includes
--------
* [Git](http://git-scm.com/)
* [NodeJS](https://nodejs.org/)
* [Gulp](http://gulpjs.com): Converts files and task running


Getting Started
---------------

* Set up your project in your code directory

git clone git@github.com/template.git your-project-folder
cd your-project-folder


* Install the dependencies

npm install


* Run the server

gulp 